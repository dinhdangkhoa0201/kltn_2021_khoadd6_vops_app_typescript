import React from 'react';
// 1. import `NativeBaseProvider` component
import {extendTheme, NativeBaseProvider} from 'native-base';
import {IPhonology} from "./models/IPhonology";
import {PhonologyComponent} from "./components/PhonologyComponent";
import {PhonologyType} from "./enums/PhonologyType";
import {Navigation} from "./app/Navigation";

export default function App() {
    const theme = extendTheme({
        colors: {
            backgroundColor: "#7dd3fc",
        },
        config: {
            initialColorMode: 'dark',
        },
    })
    return (
        <NativeBaseProvider>
            <Navigation/>
        </NativeBaseProvider>
    );
}
